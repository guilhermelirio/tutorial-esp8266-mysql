## [Tutorial] ESP8266 + MYSQL + PHP + GOOGLE CHARTS  -  Versão 2

Tutorial da criação de uma gráfico de temperatura ambiente, utilizando o Google Charts, de acordo com a temperatura cadastrada no banco de dados MySQL.

## Link do Tutorial: 
- https://medium.com/@guilhermelirio/tutorial-esp8266-mysql-php-google-charts-f20735506d4e

